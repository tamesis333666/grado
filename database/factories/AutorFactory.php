<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Autor;
use Faker\Generator as Faker;

$factory->define(Autor::class, function (Faker $faker) {
    return [
        	//CREANDO  INFORMACION FALSA PARA LA TABLA AUTOR
      'japo_nombres' => strtoupper($faker->firstNameMale) ,
      'japo_apellidos' => strtoupper($faker->lastName),
      'japo_tipo_dni' => strtoupper($faker->randomElement(['C', 'P'])),
      'japo_dni'=> $faker->numerify('##########'),
      'japo_sexo' => strtoupper($faker->randomElement(['H', 'M'])),
      'japo_fecha_autor' => $faker->date($format = 'Y-m-d', $max = 'now'),
       'japo_email'=> strtoupper($faker->email) 
    ];
});
