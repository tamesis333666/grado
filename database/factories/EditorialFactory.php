<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Editorial;
use Faker\Generator as Faker;

$factory->define(Editorial::class, function (Faker $faker) {
    return [
       //CREANDO INFORMACION FALSA PARA LA TABLA EDITORIAL
        'japo_nombre' => strtoupper($faker->company),
        'japo_email' => strtoupper($faker->unique()->safeEmail)
    ];
});
