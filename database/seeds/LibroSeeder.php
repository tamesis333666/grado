<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LibroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       //AGREGANDO INFORAMCION A LA TABLA LIBROS
        DB::table('Libros')->insert([
       		'autor_id' => '3',
       		'editorial_id' => '4',
       		'japo_ISBN' => '960 425 059 0',
       		'japo_titulo' => strtoupper('Superman'),
       		'japo_anio' => '2002',
       		'japo_precio_venta' => 75.54,
       		'japo_otros_autores' => strtoupper('Pedro Jiminez'),
       		


       ]);

        DB::table('Libros')->insert([
       		'autor_id' => '4',
       		'editorial_id' => '4',
       		'japo_ISBN' => '978-0-306-40615-7',
       		'japo_titulo' => strtoupper('Harry Poter'),
       		'japo_anio' => '1994',
       		'japo_precio_venta' => 94.10,
       		'japo_otros_autores' => strtoupper('Patricia Gonzales'),
       		


       ]);

         DB::table('Libros')->insert([
       		'autor_id' => '1',
       		'editorial_id' => '3',
       		'japo_ISBN' => '99921-58-10-7',
       		'japo_titulo' => strtoupper('Culpa Nuestra '),
       		'japo_anio' => '1987',
       		'japo_precio_venta' => 70.40,
       		'japo_otros_autores' => strtoupper('Talia Andrango'),
       		


       ]);
    }
}
