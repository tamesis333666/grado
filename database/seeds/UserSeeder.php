<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('Users')->insert([
       		'rol_id' => '1',
       		'name' =>  strtoupper('Jonathan Ponce'),
       		'email' => strtoupper('jefe@hotmail.com'),
       		'password' => bcrypt('12345678'),
       		'dni' => '1725414527',
       		'sexo' => strtoupper('H'),
       		'direccion' => strtoupper('Calderon avenida panamericana OE 40'),
          'telefono' => '0977804021',
       		'fecha_nacimiento' => '1988-04-05',


       ]);

        DB::table('Users')->insert([
       		'rol_id' => '2',
       		'name' => strtoupper('Catherine Dominguez'),
       		'email' => strtoupper('cath@gmail.com'),
       		'password' => bcrypt('12345678'),
       		'dni' => '10024871561',
       		'sexo' => 'M',
       		'direccion' => strtoupper('Cotocollado  y  Machala OE20'),
          'telefono' => '09672053441',
       		'fecha_nacimiento' => '1998-04-03',
       	

       ]);
    }
}
