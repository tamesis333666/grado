<?php

use Illuminate\Database\Seeder;
use App\Editorial;

class EditorialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       //AGREGANDO EL FACOTRY Y AGREGANDO CUANTAS VECES SE VA A REPETIR
    	
       factory(Editorial::class,15)->create();
    }
}
