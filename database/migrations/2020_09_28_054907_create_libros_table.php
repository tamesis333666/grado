<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLibrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            //MIGRACION DE AUTORES
        Schema::create('autors', function (Blueprint $table) {
            $table->id();
          
            $table->string('japo_nombres')->nullable();
            $table->string('japo_apellidos')->nullable();
            $table->string('japo_tipo_dni')->nullable();
            $table->string('japo_dni')->nullable();
            $table->string('japo_sexo')->nullable();
            $table->date('japo_fecha_autor')->nullable();
            $table->string('japo_email')->nullable();
            $table->string('japo_estado')->default('A');
            $table->timestamps();
        });
         //MIGRACION DE EDITORIAL

         Schema::create('editorials', function (Blueprint $table) {
            $table->id();
            $table->string('japo_nombre')->nullable();
            $table->string('japo_email')->nullable();
            $table->string('japo_estado')->default('A');

          
            $table->timestamps();
        });

          //MIGRACION DE LIBROS
         Schema::create('libros', function (Blueprint $table) {
            $table->id();
            $table->foreignId('autor_id')->references('id')->on('Autors')->nullable();
            $table->foreignId('editorial_id')->references('id')->on('Editorials')->nullable();
            $table->string('japo_ISBN')->nullable();
            $table->string('japo_titulo')->nullable();
            $table->string('japo_anio')->nullable();
            $table->decimal('japo_precio_venta',11,2)->nullable();
            $table->text('japo_otros_autores')->nullable();
            $table->string('japo_estado')->default('A');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //eliminacin de las tablas para hacer una migracion reset
        Schema::dropIfExists('libros');
        Schema::dropIfExists('Autors');
        Schema::dropIfExists('Editorials');
    }
}
