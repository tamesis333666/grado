-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-09-2020 a las 02:11:00
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `grado`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autors`
--

CREATE TABLE `autors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `japo_nombres` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `japo_apellidos` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `japo_tipo_dni` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `japo_dni` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `japo_sexo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `japo_fecha_autor` date DEFAULT NULL,
  `japo_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `japo_estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `autors`
--

INSERT INTO `autors` (`id`, `japo_nombres`, `japo_apellidos`, `japo_tipo_dni`, `japo_dni`, `japo_sexo`, `japo_fecha_autor`, `japo_email`, `japo_estado`, `created_at`, `updated_at`) VALUES
(1, 'ULISES', 'WOLF', 'C', '3488669165', 'H', '1998-05-05', 'ALIZE.CROOKS@GMAIL.COM', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(2, 'AUSTYN', 'GRANT', 'C', '9155452518', 'M', '1981-09-13', 'YKASSULKE@HOTMAIL.COM', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(3, 'KIM', 'SPORER', 'P', '4374964516', 'H', '2006-09-21', 'WLEFFLER@LARSON.COM', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(4, 'WELDON', 'GRANT', 'P', '8831490803', 'M', '1990-02-01', 'BRAKUS.MAEVE@SHIELDS.ORG', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(5, 'CHRISTOP', 'HAGENES', 'P', '3720730900', 'H', '1985-01-11', 'MELANY.ROBEL@WINTHEISER.COM', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(6, 'GIOVANI', 'EMMERICH', 'P', '8600287695', 'H', '1994-08-27', 'AMARA68@KIHN.BIZ', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(7, 'LISANDRO', 'DUBUQUE', 'P', '9151735008', 'H', '1981-07-09', 'VITA.JOHNSTON@GMAIL.COM', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(8, 'JERAMY', 'GOLDNER', 'C', '5247373890', 'M', '2007-09-30', 'PLUETTGEN@BARTELL.COM', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(9, 'KEYON', 'DIETRICH', 'C', '5363162462', 'M', '1996-09-16', 'YOST.KOBY@YAHOO.COM', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(10, 'CHET', 'SAUER', 'P', '4049381744', 'M', '1986-01-21', 'ALENE.JACOBI@YAHOO.COM', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(11, 'ISMAEL', 'ABSHIRE', 'C', '2898350507', 'H', '2019-01-09', 'EKOVACEK@GMAIL.COM', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(12, 'ALAN', 'KOVACEK', 'P', '7275408206', 'M', '2013-12-17', 'THAMILL@GMAIL.COM', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(13, 'JOE', 'SCHIMMEL', 'C', '6503544143', 'H', '1992-12-14', 'MYRL81@ORN.NET', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(14, 'RICK', 'SPORER', 'C', '9948826943', 'M', '1979-07-12', 'MAGDALEN.LEDNER@QUIGLEY.COM', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(15, 'NATHANIAL', 'HIRTHE', 'P', '8753925591', 'M', '2009-01-10', 'NMOHR@GMAIL.COM', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `editorials`
--

CREATE TABLE `editorials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `japo_nombre` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `japo_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `japo_estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `editorials`
--

INSERT INTO `editorials` (`id`, `japo_nombre`, `japo_email`, `japo_estado`, `created_at`, `updated_at`) VALUES
(1, 'HYATT-HAGENES', 'KOVACEK.ARACELY@EXAMPLE.COM', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(2, 'NADER-HODKIEWICZ', 'KGIBSON@EXAMPLE.ORG', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(3, 'MONAHAN, DUBUQUE AND GULGOWSKI', 'SPARKER@EXAMPLE.ORG', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(4, 'HERZOG, BRUEN AND DOUGLAS', 'SUNNY.GLEASON@EXAMPLE.NET', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(5, 'BRUEN-HOEGER', 'VGERHOLD@EXAMPLE.ORG', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(6, 'FAHEY LLC', 'JVONRUEDEN@EXAMPLE.NET', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(7, 'FLATLEY-GOYETTE', 'DANGELO56@EXAMPLE.COM', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(8, 'KILBACK LLC', 'ZIEMANN.KEELY@EXAMPLE.COM', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(9, 'MEDHURST, BERGSTROM AND RUSSEL', 'NKAUTZER@EXAMPLE.NET', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(10, 'KRAJCIK LTD', 'RAU.JEFFRY@EXAMPLE.NET', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(11, 'BROWN, GISLASON AND BARTON', 'DANYKA80@EXAMPLE.COM', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(12, 'TILLMAN-STANTON', 'MBEAHAN@EXAMPLE.ORG', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(13, 'GRIMES, MAGGIO AND MCDERMOTT', 'EUGENE85@EXAMPLE.NET', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(14, 'MANN INC', 'KSTRACKE@EXAMPLE.NET', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35'),
(15, 'VON, EFFERTZ AND FADEL', 'ADAN.HAYES@EXAMPLE.ORG', 'A', '2020-09-29 05:09:35', '2020-09-29 05:09:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libros`
--

CREATE TABLE `libros` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `autor_id` bigint(20) UNSIGNED NOT NULL,
  `editorial_id` bigint(20) UNSIGNED NOT NULL,
  `japo_ISBN` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `japo_titulo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `japo_anio` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `japo_precio_venta` decimal(11,2) DEFAULT NULL,
  `japo_otros_autores` text COLLATE utf8mb4_unicode_ci,
  `japo_estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `libros`
--

INSERT INTO `libros` (`id`, `autor_id`, `editorial_id`, `japo_ISBN`, `japo_titulo`, `japo_anio`, `japo_precio_venta`, `japo_otros_autores`, `japo_estado`, `created_at`, `updated_at`) VALUES
(1, 3, 4, '960 425 059 0', 'SUPERMAN', '2002', '75.54', 'PEDRO JIMINEZ', 'A', NULL, NULL),
(2, 4, 4, '978-0-306-40615-7', 'HARRY POTER', '1994', '94.10', 'PATRICIA GONZALES', 'A', NULL, NULL),
(3, 1, 3, '99921-58-10-7', 'CULPA NUESTRA ', '1987', '70.40', 'TALIA ANDRANGO', 'A', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2014_10_12_000000_create_users_table', 1),
(12, '2014_10_12_100000_create_password_resets_table', 1),
(13, '2019_08_19_000000_create_failed_jobs_table', 1),
(14, '2020_09_28_054907_create_libros_table', 1),
(15, '2020_09_28_054931_create_rols_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rols`
--

CREATE TABLE `rols` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `japo_nombre` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `japo_estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `rols`
--

INSERT INTO `rols` (`id`, `japo_nombre`, `japo_estado`, `created_at`, `updated_at`) VALUES
(1, 'ADMINISTRADOR', 'A', NULL, NULL),
(2, 'VENDEDOR', 'A', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rol_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dni` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sexo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `rol_id`, `name`, `email`, `email_verified_at`, `password`, `dni`, `sexo`, `direccion`, `telefono`, `fecha_nacimiento`, `foto`, `estado`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'JONATHAN PONCE', 'JEFE@HOTMAIL.COM', NULL, '$2y$10$oL.T80FL/k1fS5S63tkBK.0iHXVifj3.3QDaxJ6RmKYK0/hN8THFi', '1725414527', 'H', 'CALDERON AVENIDA PANAMERICANA OE 40', '0977804021', '1988-04-05', NULL, 'A', NULL, NULL, NULL),
(2, 2, 'CATHERINE DOMINGUEZ', 'CATH@GMAIL.COM', NULL, '$2y$10$eirYqWXifZfaEVv0nU23ZOJj9uXRoozL8wwhdhu/lagc52ocekvPe', '10024871561', 'M', 'COTOCOLLADO  Y  MACHALA OE20', '09672053441', '1998-04-03', NULL, 'A', NULL, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `autors`
--
ALTER TABLE `autors`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `editorials`
--
ALTER TABLE `editorials`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `libros`
--
ALTER TABLE `libros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `libros_autor_id_foreign` (`autor_id`),
  ADD KEY `libros_editorial_id_foreign` (`editorial_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `rols`
--
ALTER TABLE `rols`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `autors`
--
ALTER TABLE `autors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `editorials`
--
ALTER TABLE `editorials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `libros`
--
ALTER TABLE `libros`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `rols`
--
ALTER TABLE `rols`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `libros`
--
ALTER TABLE `libros`
  ADD CONSTRAINT `libros_autor_id_foreign` FOREIGN KEY (`autor_id`) REFERENCES `autors` (`id`),
  ADD CONSTRAINT `libros_editorial_id_foreign` FOREIGN KEY (`editorial_id`) REFERENCES `editorials` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
