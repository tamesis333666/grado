@extends('layouts.plantilla')

@section('mensaje')

	
	  @if(Session::has('Mensaje'))  
	      <div class="alert alert-warning alert-dismissible mt-3">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	        <h5><i class="icon fas fa-warning"></i> Alert!</h5>
	        {{ Session::get('Mensaje') }}
	      </div>
	    @endif
@endsection
@section('content')

 <div class="row gutter-xs">
            <div class="col-xs-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                <a href="{{ route('autor.create') }}" class="btn btn-outline-warning ">Agregar</a>
             	 </div>
                   
                  </div>
                  <strong>Mantenimiento de Autores</strong>
                </div>
                <div class="card-body">
                  <table id="demo-datatables-5" class="table table-striped table-bordered table-nowrap dataTable" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>Nombres y Apellidos</th>                                 
                        <th >DNI.</th>
                        <th >Sexo</th>
                        <th >Fecha Nacim.</th>
                        <th >Email</th>
                        <th >Estado</th>
                        <th>Eliminar</th>
                        <th>Editar</th>
                      </tr>
                    </thead>
                    <tbody>
                     @foreach($leer as $recorrer)
                        <tr>
                            <td>{{ $recorrer->japo_nombres }} {{ $recorrer->japo_apellidos }}</td>                               
                            <td>{{ $recorrer->japo_dni }}</td>
                            <td>{{ $recorrer->japo_sexo }}</td>
                            <td>{{ $recorrer->japo_fecha_autor }}</td>
                            <td>{{ $recorrer->japo_email }}</td>
                            <td>{{ $recorrer->japo_estado }}</td>

                            <td>
                                <form action="{{ route('autor.destroy',$recorrer->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                      <button class="btn btn" style="border:none; background:white;" onclick="return confirm('Desea Eliminar?')">
                                      	
					                      <span class="icon icon-trash"></span>
					                      <span class="caption">trash</span>
					                    
                                      </button>
                                     
                                </form> 
                                
                            </td>
                            <td>  
                             

                                                     
                                <a class="btn btn-info btn-icon" href="{{ route('autor.edit',$recorrer->id) }}">   <span class="icon icon-edit"></span>
                     			      <span class="caption">edit</span>
                     			    </a>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                  
                  </table>
                </div>
              </div>
            </div>
          </div>



@endsection