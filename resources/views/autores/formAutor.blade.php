@include("recursos.validacion")



  					 <div class="form-group">
		                <label class="col-md-3 control-label" for="demo-text-input">Nombres</label>
		                    <div class="col-md-9">
		                        <input type="text" required=""  id="nombres" value="{{ old('japo_nombres') }}" name="japo_nombres"  id="demo-text-input" class="form-control" placeholder="Nombres">
		                        @error('japo_nombres')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>
		
		                <!--Text Input-->
		                <div class="form-group">
		                    <label class="col-md-3 control-label" for="demo-text-input">Apellidos</label>
		                    <div class="col-md-9">
		                        <input type="text"  id="apellidos" value="{{ old('japo_apellidos') }}" name="japo_apellidos" id="demo-text-input" class="form-control" placeholder="Apellidos">
		                        @error('japo_apellidos')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>
		
		                <!--Email Input-->
		                <div class="form-group">
		                    <label class="col-md-3 control-label" for="demo-email-input">Tipo DNI</label>
		                    <div class="col-md-9">
		                        <select required="" name="japo_tipo_dni" class="form-control">
		                        	<option value="">Seleccion tipo de dni</option>	
		                        	<option value="C" >Cedula Ciudadania</option>
		                        	<option value="P" >Pasaporte</option>
		                        </select>

		                        @error('japo_tipo_dni')
									<span class="invalid-feedback d-block" role="alert">
										{{ $message }}
									</span>
									
								@enderror	
		                        
		                    </div>
		                </div>

		                 <div class="form-group">
		                    <label class="col-md-3  control-label" for="demo-text-input">Dni</label>
		                    <div class="col-md-9">
		                        <input type="number" value="{{ old('japo_dni') }}" required="" id="documento" onblur="validarDocumento()" name="japo_dni"  onKeyPress="return soloNumeros(event)"  id="demo-text-input" class="form-control" placeholder="Ingrese Dni">
		                         @error('japo_dni')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>

		                <div class="form-group">
		                    <label class="col-md-3 control-label" for="demo-email-input">Sexo</label>
		                    <div class="col-md-9">
		                        <select required="" name="japo_sexo" class="form-control">
		                        	<option value="">Seleccion tipo de sexo</option>	
		                        	<option value="H">Hombre</option>
		                        	<option value="M">Mujer</option>
		                        </select>

	                           @error('japo_sexo')
								<span class="invalid-feedback d-block" role="alert">
									{{ $message }}
								</span>
								@enderror	
		                       
		                    </div>
		                </div>

		                 <div class="form-group">
		                    <label class="col-md-3 control-label" for="demo-text-input">Fecha Nacimiento(yyyy-mm-dd)</label>
		                    <div class="col-md-9">
		                        <input type="text" required=""  id="fecha" value="{{ old('japo_fecha_autor') }}"  name="japo_fecha_autor"   id="demo-text-input" class="form-control" placeholder="Ingrese fecha">
		                        @error('japo_fecha_autor')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>
		

		                 <div class="form-group">
		                    <label class="col-md-3 control-label" for="demo-text-input">Email</label>
		                    <div class="col-md-9">
		                        <input type="text" required="" id="email" value="{{ old('japo_email') }}" name="japo_email"    id="demo-text-input" class="form-control" placeholder="Ingrese Email">
		                        @error('japo_email')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>         
								              
								              
					               
					<div class="modal-footer">
	                    <a href="{{ route('autor.index') }}" class="btn btn-default" type="button">Close</a>
	                    <input type="submit"  class="btn btn-primary"  onclick="return confirm('Desea Agregar?')" value="Crear Actor" name="">
                    
               		 </div>

               		

               		