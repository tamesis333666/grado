@include('layouts.header')

    <div class="layout-main">
    @include('layouts.navbar')
      <div class="layout-content">
        <div class="layout-content-body">
        
          
       
               
          
          <div class="text-center m-b">
            <h3 class="m-b-0">Biblioteca Ponce</h3>
            <small>@yield('mensaje')</small>
          </div>
         @yield('content')
        </div>
      </div>


@include('layouts.footer')
