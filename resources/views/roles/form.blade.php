		 <div class="form-group">
            <label class="col-md-3 control-label" for="demo-text-input">Nombre Rol</label>
                <div class="col-md-9">
                    <input type="text" required="" maxlength="20" minlength="4" name="japo_nombre" id="rol"   class="form-control" placeholder="Ingrese nombre de rol">
                    @error('japo_nombre')
                    <small class="help-block text-danger">{{ $message }}</small>
                    @enderror
                </div>
         </div>	
		      
              <br>        
						              
		  <div class="m-t-lg">
                    <input class="btn btn-warning" type="submit" onclick="return confirm('Desea Agregar?')" value="{{ $mensaje === 'crear' ? 'Guardar ' : ' Editar' }} Rol" name="">
                    <button class="btn btn-default" data-dismiss="modal"  type="button">Cancel</button>
         </div>              
		