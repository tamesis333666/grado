	 <div class="form-group">
        <label class="col-md-3 control-label" for="demo-text-input">Nombre Editorial</label>
            <div class="col-md-9">
                <input type="text" id="editorial" required="" value="{{ old('japo_nombre') }}" name="japo_nombre"  id="demo-text-input" class="form-control" placeholder="Ingrese Editorial">
                @error('japo_nombre')
                <small class="help-block text-danger">{{ $message }}</small>
                @enderror
            </div>
        </div>

     
    <div class="form-group">
            <label class="col-md-3 control-label" for="demo-text-input">Email</label>
            <div class="col-md-9">
                <input type="text" id="email" required=""  value="{{ old('japo_email') }}" name="japo_email"    id="demo-text-input" class="form-control" placeholder="Ingrese Email">
                @error('japo_email')
                <small class="help-block text-danger">{{ $message }}</small>
                @enderror
            </div>
    </div>
	
	              
	
	              
	
	              
	               
	 <div class="modal-footer">
        <a href="{{ route('editorial.index') }}" class="btn btn-default" type="button">Close</a>
        <input type="submit" class="btn btn-primary"  onclick="return confirm('Desea Agregar?')" value="Crear Editorial" name="">
    
	 </div>