@extends('layouts.plantilla')

@section('mensaje')

	

@section('content')
  

   <div class="col-lg-12 ">
              <div class="panel">
                  <div class="panel-heading">
                      <h3 class="panel-title">Editar Informacion de la Editorial</h3>
                  </div>
      
      
                  <!-- BASIC FORM ELEMENTS -->
                  <!--===================================================-->
                  <form  action="{{ route('editorial.update', $datoIndividual->id) }}" method="post" class="panel-body form-horizontal form-padding">
                    @csrf
                    @method('PUT')
      
                                 <div class="form-group">
                    <label class="col-md-3 control-label" for="demo-text-input">Nombre Editorial</label>
                        <div class="col-md-9">
                            <input type="text" id="editorial" name="japo_nombre" value="{{ isset($datoIndividual->japo_nombre) ? $datoIndividual->japo_nombre : '' }}" id="demo-text-input" class="form-control" placeholder="Ingrese nombre editorial">
                            @error('japo_nombre')
                            <small class="help-block text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
    
                  

                     <div class="form-group">
                        <label class="col-md-3 control-label" for="demo-text-input">Email</label>
                        <div class="col-md-9">
                            <input type="text" id="email" name="japo_email" value="{{ isset($datoIndividual->japo_email) ? $datoIndividual->japo_email : ''  }}"   id="demo-text-input" class="form-control" placeholder="Ingrese Email">
                            @error('japo_email')
                            <small class="help-block text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="demo-email-input">Estado</label>
                        <div class="col-md-9">
                            <select name="japo_estado" class="form-control">
                              <option>Seleccion de estado</option>  
                              <option value="A" @if($datoIndividual->japo_estado == 'A') selected=""  @endif) >Activo</option>
                              <option value="I" @if($datoIndividual->japo_estado == 'I') selected=""  @endif) >Inactivo</option>
                            </select>
                            @error('japo_estado')
                            <small class="help-block text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
          
                        
          
                        
          
                        
                         
           <div class="modal-footer">
                      <a href="{{ route('editorial.index') }}" class="btn btn-default" type="button">Close</a>
                      <input type="submit" class="btn btn-primary"  onclick="return confirm('Desea Actualizar?')" value="Actualizar Editorial" name="">
                    
                   </div>
          </form>
                  <!--===================================================-->
                  <!-- END BASIC FORM ELEMENTS -->
      
      
              </div>
          </div>
       


@endsection