				<div class="form-group">
	                <label class="col-md-3 control-label" for="demo-text-input">Autor</label>
	                    <div class="col-md-9">
	                        <select class="form-control" required="" name="autor_id">
	                        	@forelse($autores as $autor)
	                        	
	                        		<option value="{{ $autor->id }}">{{ $autor->japo_nombres .' '. $autor->japo_apellidos}}</option>
	                        	@empty
	                        	<span class="text-danger">No se encontraron roles</span>

	                        	@endforelse
	                        </select>
	                        @error('autor_id')
	                        <small class="help-block text-danger">{{ $message }}</small>
	                        @enderror
	                    </div>
	             </div>

	             <div class="form-group">
	                <label class="col-md-3 control-label" for="demo-text-input">Editorial</label>
	                    <div class="col-md-9">
	                        <select class="form-control" required="" name="editorial_id">
	                        	@forelse($editoriales as $editorial)
	                        	
	                        		<option value="{{ $editorial->id }}">{{ $editorial->japo_nombre }}</option>
	                        	@empty
	                        	<span class="text-danger">No se encontraron roles</span>

	                        	@endforelse
	                        </select>
	                        @error('editorial_id')
	                        <small class="help-block text-danger">{{ $message }}</small>
	                        @enderror
	                    </div>
	             </div>


	          
				<div class="form-group">
               		 <label class="col-md-3 control-label" for="demo-text-input">ISBN</label>
               		  
                    <div class="col-md-9">
                        <input type="text" name="japo_ISBN"  required="" value="{{ old('japo_ISBN') }}"  id="isbn" onblur="run()"   class="form-control" placeholder="Ingrese ISBN">
                        @error('japo_ISBN')
                        <small class="help-block text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="form-group">
               		 <label class="col-md-3 control-label" for="demo-text-input">Titulo</label>
                    <div class="col-md-9">
                        <input type="text" name="japo_titulo"  id="titulo" value="{{ old('japo_titulo') }}" class="form-control" placeholder="Ingrese el titulo del libro">
                        @error('japo_titulo')
                        <small class="help-block text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>


                <div class="form-group">
               		 <label class="col-md-3 control-label" for="demo-text-input">Año</label>
                    <div class="col-md-9">
                        <input type="number" name="japo_anio"  value="{{ old('japo_anio') }}" id="anio" class="form-control" placeholder="Ingrese el año del libro">
                        @error('japo_anio')
                        <small class="help-block text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>


                
		             
	            <div class="form-group">
	                    <label class="col-md-3 control-label" for="demo-text-input">Precio de venta</label>
	                    <div class="col-md-9">
	                        <input type="text" name="japo_precio_venta" value="{{ old('japo_precio_venta') }}"    id="precio" class="form-control" placeholder="Ingrese precio de venta">
	                        @error('japo_precio_venta')
	                        <small class="help-block text-danger">{{ $message }}</small>
	                        @enderror
	                    </div>
	            </div>
					
			  <div class="form-group">
                    <label class="col-md-3 control-label"  for="demo-text-input">Otros autores</label>
                    <div class="col-md-9">
                        <input  name="japo_otros_autores"  value="No hay"  id="autor" class="form-control" placeholder="Ingrese otros autores">
                        @error('japo_otros_autores')
                        <small class="help-block text-danger">{{ $message }}</small>
                        @enderror
                    </div>
	            </div>
						              
					 
					              
					
					              
				               
				 <div class="modal-footer">
	                    <a href="{{ route('libro.index') }}" class="btn btn-default" type="button">Close</a>
	                    <input type="submit" class="btn btn-primary"  onclick="return confirm('Desea Agregar?')" value="Crear Libro" name="">
                    
               	</div>
