@extends('layouts.plantilla')



  

@section('content')
  
      <div class="col-lg-12 ">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Aumentar precio del libro  {{ $actualizar->japo_titulo }}
                  Valor $ {{ $actualizar->japo_precio_venta }}
                </h3>

            </div>


       
            <!--===================================================-->
            <form  action="{{ route('aumentopreciolibro.update', $actualizar->id) }}" onsubmit="return validacionPorcentaje()"  method="post" class="panel-body form-horizontal form-padding">
              @csrf
              @method('PUT')
              @include('libros.formAumentarPorcentaje',['mensaje'=>'aumentar'])
             </div>
            </form>
            <!--===================================================-->
            <!-- END BASIC FORM ELEMENTS -->


        </div>
    </div>

@endsection