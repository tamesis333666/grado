        <div class="form-group">
             <label class="col-md-4 control-label" for="demo-text-input">Porcentaje</label>
            <div class="col-md-8">
                <input type="text" required="" name="japo_porcentaje"  id="porcentaje"  class="form-control" placeholder="Ingrese Procentaje">
                @error('japo_porcentaje')
                <small class="help-block text-danger">{{ $message }}</small>
                @enderror
            </div>

             <div class="col-md-8">
                <input type="hidden" name="japo_titulo"  value="{{ $actualizar->japo_titulo }}" id="demo-text-input" class="form-control" placeholder="Ingrese Procentaje Ejem. 10,20">
                @error('japo_titulo')
                <small class="help-block text-danger">{{ $message }}</small>
                @enderror
            </div>

        </div>

         <div class="modal-footer">
            <a href="{{ route('libro.index') }}" class="btn btn-default" type="button">Close</a>
            <input type="submit" onclick="return confirm('Desea cambiar el porcentaje?')" class="btn btn-warning"  value="{{ $mensaje === 'aumentar' ? 'Aumentar ' : ' Disminuir' }} Porcentaje del libro" name="">
        
        </div>