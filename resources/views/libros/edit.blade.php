@extends('layouts.plantilla')



@section('content')
      
    <div class="col-lg-12 ">
              <div class="panel">
                  <div class="panel-heading">
                      <h3 class="panel-title bg-primary">Editar Informacion del libro</h3>
                  </div>
      
      
             
                  <!--===================================================-->
                  <form  action="{{ route('libro.update', $actualizar->id) }}"  method="post" class="panel-body form-horizontal form-padding">
                    @csrf
                    @method('PUT')


               
           


               <div class="form-group">
                  <label class="col-md-3 control-label" for="demo-text-input">Autor</label>
                      <div class="col-md-9">
                          <select class="form-control" name="autor_id">
                            @forelse($autores as $autor)
                            
                              <option value="{{ $autor->id }}" @if($autor->id == $actualizar->autor_id) selected="" @endif>{{ $autor->japo_nombres. ' '.$autor->japo_apellidos }}</option>
                            @empty
                            <span class="text-danger">No se encontraron roles</span>

                            @endforelse
                          </select>
                          @error('autor_id')
                          <small class="help-block text-danger">{{ $message }}</small>
                          @enderror
                      </div>
                 </div> 


         
                 <div class="form-group">
                    <label class="col-md-3 control-label" for="demo-text-input">Seleccione Editorial</label>
                        <div class="col-md-9">
                            <select class="form-control" name="editorial_id">
                              @forelse($editoriales as $editorial)
                              
                                <option value="{{ $editorial->id }}" @if($editorial->id == $actualizar->editorial_id) selected="" @endif>{{ $editorial->japo_nombre }}</option>


                              @empty
                              <span class="text-danger">No se encontraron roles</span>

                              @endforelse
                            </select>
                            @error('editorial_id')
                            <small class="help-block text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                 </div>

      
                  <div class="form-group">
                        <label class="col-md-3 control-label" for="demo-text-input">ISBN</label>
                          <div class="col-md-9">
                              <input type="text" required="" name="japo_ISBN" id="isbn" onblur="run()" value="{{ isset($actualizar->japo_ISBN) ? $actualizar->japo_ISBN : '' }}" id="demo-text-input" class="form-control" placeholder="Ingrese ISBN">
                              @error('japo_ISBN')
                              <small class="help-block text-danger">{{ $message }}</small>
                              @enderror
                          </div>
                    </div>
    
                  

                     <div class="form-group">
                        <label class="col-md-3 control-label" for="demo-text-input">Titulo</label>
                        <div class="col-md-9">
                            <input type="text" id="titulo" name="japo_titulo" value="{{ isset($actualizar->japo_titulo) ? $actualizar->japo_titulo : ''  }}"   id="demo-text-input" class="form-control" placeholder="Ingrese titulo">
                            @error('japo_titulo')
                            <small class="help-block text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="col-md-3 control-label" for="demo-text-input">Año</label>
                        <div class="col-md-9">
                            <input type="text" id="anio" name="japo_anio" value="{{ isset($actualizar->japo_anio) ? $actualizar->japo_anio : ''  }}"   id="demo-text-input" class="form-control" placeholder="Ingrese Año del libro">
                            @error('japo_anio')
                            <small class="help-block text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="col-md-3 control-label" for="demo-text-input">Precio de venta</label>
                        <div class="col-md-9">
                            <input type="text" id="precio" name="japo_precio_venta" value="{{ isset($actualizar->japo_precio_venta) ? $actualizar->japo_precio_venta : ''  }}"   id="demo-text-input" class="form-control" placeholder="Ingrese Precio de venta">
                            @error('japo_precio_venta')
                            <small class="help-block text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                       <div class="form-group">
                        <label class="col-md-3 control-label" for="demo-text-input">Otros autores</label>
                        <div class="col-md-9">
                            <input  name="japo_otros_autores"  style="resize: none" value="{{ isset($actualizar->japo_otros_autores) ? $actualizar->japo_otros_autores : ''  }}"   id="demo-text-input" class="form-control" placeholder="Ingrese otros autores">
                            @error('japo_otros_autores')
                            <small class="help-block text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="demo-email-input">Estado</label>
                        <div class="col-md-9">
                            <select name="japo_estado" class="form-control">
                              <option>Seleccion de estado</option>  
                              <option value="A" @if($actualizar->japo_estado == 'A') selected=""  @endif) >Activo</option>
                              <option value="I" @if($actualizar->japo_estado == 'I') selected=""  @endif) >Inactivo</option>
                            </select>
                            @error('japo_estado')
                            <small class="help-block text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
          
                        
          
                        
          
                        
                         
           <div class="modal-footer">
                      <a href="{{ route('libro.index') }}" class="btn btn-default" type="button">Close</a>
                      <input type="submit" class="btn btn-primary"  onclick="return confirm('Desea Actualizar?')" value="Actualizar Libro" name="">
                    
                   </div>
                  </form>
                  <!--===================================================-->
                  <!-- END BASIC FORM ELEMENTS -->
      
      
              </div>
    </div>



@endsection