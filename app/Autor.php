<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
   //LARAVEL ADMITIRA SOLO ESTOS CAMPOS EN  EL INGRESO DEL FORMULARIO PARA LA TABLA AUTOR EVITANDO ATAQUES SQL
     protected $fillable= ['japo_nombres', 'japo_apellidos','japo_tipo_dni','japo_dni','japo_sexo','japo_fecha_autor','japo_email','japo_estado'];
}
