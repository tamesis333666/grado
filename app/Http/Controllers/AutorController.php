<?php

namespace App\Http\Controllers;

use App\Autor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AutorController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $autor = Autor::all();
        $leer = $autor;
        return view('autores.index',compact('leer'));
    }

      public function create()
    {
         return view('autores.create');
    }
    
    public function store(Request $request)
    {
    $datos = [
            'japo_nombres' => 'required | min:5 | max:30 | regex:/(?!.* $)[A-Z][a-z ]+/',
            'japo_apellidos' => 'required |min:5 | max:30 | string',
            'japo_tipo_dni' => 'required ',
            'japo_dni' => 'required | alpha_num | max:15 | min:10 | unique:autors',
            'japo_sexo' => 'required|',
            'japo_fecha_autor' => 'required | date',
            'japo_email' => 'required | email | min:5 | max:40 | string | unique:autors'
        ];
      
      $this->validate($request,$datos);
        $datosAutor=request()->except('_token');
         $datosAutor['japo_nombres'] = strtoupper($request->get('japo_nombres'));
         $datosAutor['japo_apellidos'] = strtoupper($request->get('japo_apellidos'));
         $datosAutor['japo_email'] = strtoupper($request->get('japo_email'));
        
         
        Autor::insert($datosAutor);
       
        return redirect('/autor')->with([
            'Mensaje' => 'Autor Agregado'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Autor  $autor
     * @return \Illuminate\Http\Response
     */
    public function show(Autor $autor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Autor  $autor
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
      
       $actualizar =  Autor::findOrFail($id);
        return view('autores.edit',compact('actualizar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Autor  $autor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        
       
     $datos = [
            'japo_nombres' => 'required | min:5 | max:30 | string',
            'japo_apellidos' => 'required |min:5 | max:30 | string',
            'japo_tipo_dni' => 'required ',
            'japo_dni' => 'required | alpha_num | max:15 | min:10 ',
            'japo_sexo' => 'required|',
            'japo_fecha_autor' => 'required | date',
            'japo_email' => 'required | email | min:5 | max:40 | string ',
            'japo_estado' => 'required'
        ];
      
      $this->validate($request,$datos);


       $datosAutor = request()->except('_token','_method'); 
       $datosAutor['japo_nombres'] = strtoupper($request->get('japo_nombres'));
       $datosAutor['japo_apellidos'] = strtoupper($request->get('japo_apellidos'));
       $datosAutor['japo_email'] = strtoupper($request->get('japo_email')); 
       
       Autor::whereId($id)->update($datosAutor);
       
       return redirect("autor")->with([
        'Mensaje' => 'Autor actualizado'
       ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Autor  $autor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

         $libro = DB::table('libros' )
     ->join('Autors','libros.autor_id','=','Autors.id')
   
      ->select('Autors.japo_nombres','Autors.japo_apellidos','Autors.id as id del autor','libros.autor_id','libros.japo_titulo')
      
        ->where('libros.autor_id','=',$id)
        ->get();

        if(count($libro) <= 0){
          
            Autor::whereId($id)->delete();
       
            return redirect('/autor')->with([
            'Mensaje' => 'Autor Eliminado',
             ]);
      
        }
        else {
            $recuperar = Autor::findOrFail($id);
             $autor = $recuperar['japo_nombres'] .' '. $recuperar['japo_apellidos'];
             return redirect('/autor')->with([
            'Mensaje' => 'No se puede borrar el autor. Elimine  los libros que esten referenciados al actor '.$autor,
             ]);
        }
         

       
        



        
        
    }
}
