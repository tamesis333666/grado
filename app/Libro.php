<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    //LARAVEL ADMITIRA SOLO ESTOS CAMPOS EN  EL INGRESO DEL FORMULARIO PARA LA TABLA LIBRO EVITANDO ATAQUES SQL
    protected $fillable=['autor_id','editorial_id','japo_ISBN','japo_titulo','japo_anio','japo_precio_venta','japo_otros_autores','japo_estado'];

    //RELACION  DE UNO A MUCHOS

    public function autor(){
        return $this->belongsTo('App\Autor');
    }
    //RELACION  DE UNO A MUCHOS

    public function editorial(){
    	return $this->belongsTo('App\Editorial');
    }

}
